#!/bin/bash -l



module load bioinfo-tools
module load SLiM/2.6 


burn=$1
tend=$2
slim_file=$3
num=$4

PREFIX2="Round_$SLURM_ARRAY_TASK_ID"
     mkdir $PREFIX2
        cp $burn  $PREFIX2
        cd $PREFIX2

 for ((i=1; i<=1000; i+=1))
        do  
          Seed=$(python -c 'import random as R; print(R.randint(1, 2**32-1))')
                slim -seed $Seed -d "BI='${burn}'" ./../$slim_file
                
        
                
                mv inversion_data_fm.txt ${i}_inversion_data_fm.txt
                mv fitness_p1.txt ${i}_fitness_p1.txt
                mv inv_counts.txt ${i}_inv_counts.txt
                mv final_population.txt ${i}_final_population.txt
                mv del_mutations.txt ${i}_del_mutations.txt
                
                time_end=$(awk 'NR==1{print $2}' ${i}_del_mutations.txt)
                inv_count=0
                if [[ $time_end == $tend ]]; then 
                        inv_count=$(awk 'END {print $12}' ${i}_inv_counts.txt)
                fi
                
echo "$Seed,$num,$PREFIX2,$i,$time_end,$inv_count" >> ./../../master_file_BL.csv

        
done 


