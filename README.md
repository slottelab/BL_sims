# Balanced Lethal Simulations

For this project, we used Slim version 2.6 (Haller). The corresponding manuscript can be found on bioRxiv doi:[10.1101/2021.11.27.470204](https://www.biorxiv.org/content/10.1101/2021.11.27.470204v1). 

# Burn in
The burn-ins can be recreated by running one of the four burn-ins (200k or 500k generations and with fully recessive or partially recessive deleterious mutations).

[**BL_burn_in_200K_h0**](https://gitlab.com/slottelab/BL_sims/-/blob/master/BL_burn_in_200K_h0) 

[**BL_burn_in_200K_h0.1**](https://gitlab.com/slottelab/BL_sims/-/blob/master/BL_burn_in_200K_h0.1) 

[**BL_burn_in_500K_h0**](https://gitlab.com/slottelab/BL_sims/-/blob/master/BL_burn_in_500K_h0) 

[**BL_burn_in_500K_h0.1**](https://gitlab.com/slottelab/BL_sims/-/blob/master/BL_burn_in_500K_h0.1) 


We also provide the output of the burn-ins that we used for the main simulations. We ran 10 reps for each parameter set and all burn-ins (in a compressed folder) are found [**here**](https://gitlab.com/slottelab/BL_sims/-/blob/master/BIs.zip).

# Simulations

We ran 21 sets of parameters in total and all SLiM scripts (in a compressed folder) are found [**here**](https://gitlab.com/slottelab/BL_sims/-/blob/master/Slim_Scripts.zip)

We provide a directly available [**example**](https://gitlab.com/slottelab/BL_sims/-/blob/master/BL_200k_h0.1_gc1_2500) for h=0.1, gc=1, and N=2,500.

# Running simulations on a cluster

The bash scripts [**main_script.sh**](https://gitlab.com/slottelab/BL_sims/-/blob/master/main_script.sh) and [**sub_script.sh**](https://gitlab.com/slottelab/BL_sims/-/blob/master/sub_script.sh) will create a set of folders and run 100,000 reps of a specific parameter combination spread across 10 different burn-ins. Both scripts should be run in a folder with the correct SLiM script and burn-in outputs (all 10 for the parameter set). The syntax for running (in Slurm) is

sbatch main_script.sh gc (gc0 or gc1) h (h0.1 or h0) pop_size (100, 250, 500 or 2500) length_of_BI (200k or 500k) time_end (400000 or 700000)

example:

sbatch main_script.sh gc0 h0.1 2500 200k 400001

This will create the folder BL_200k_h0.1_gc0_2500_runs. In this folder will be the master_file_BL.csv file containing the seed,   burn_in file used, folder name, rep, time that the simulation ended, and number of inverted arrangements present at the end of the simulation. The main folder contains ten sub-folders named the same with the addition _1 to _10 that contain the runs for each specific burn-in. Each of these sub-sub-folders will contain 10 folders called Round_1 to Round_10 that contain the raw output from each thread, for a given burn-in.

# Initial analysis on a cluster

The R script [**aod.R**](https://gitlab.com/slottelab/BL_sims/-/blob/master/aod.R) should be put in the main folder (e.g., BL_200k_h0.1_gc0_2500_runs). The beginning of the file should be edited to reflect:

N_ind=2500 (the number of individuals)

Last_g=400001 (the final generation)

Gen_max=400001 (the final generation)

time_start=200000 (the length of the burn-in, the first generation of the main simulation)

base="BL_200k_h0.1_gc1_2500" (folder name)

source("new_MBI.R") (location of [**new_MBI.R**](https://gitlab.com/slottelab/BL_sims/-/blob/master/new_MBI.R))

aod.R should be run to generate the preliminary analysis which will generate an updated master file with the suffix master_file_improved.csv. 


# Secondary analysis on the desktop

The analysis was conducted in R. The main results and figures are presented in an R notebook, [**slim_BL_secondary_analysis.Rmd**](https://gitlab.com/slottelab/BL_sims/-/blob/master/slim_BL_secondary_analysis.Rmd). These scripts run on the  updated master files found (in a compressed folder) [**here**](https://gitlab.com/slottelab/BL_sims/-/blob/master/Results.zip).
