#!/bin/bash -l


gc=$1
h=$2
pop=$3
len=$4
time_end=$5
dir="BL_${len}_${h}_${gc}_${pop}_runs"
slim_file="BL_${len}_${h}_${gc}_${pop}"

       mkdir $dir
        cp aod.R $dir
        cp R.sh $dir
        cd $dir
        echo "seed, burn_in, folder, rep, time_end, inv_count" > master_file_BL.csv


 for ((i=1; i<=10; i+=1))
        do  
dir2="BL_${len}_${h}_${gc}_${pop}_${i}_runs"        
        mkdir $dir2
        cp ./../$slim_file $dir2
        cp ./../burn_in_${len}_${h}_${i}.txt $dir2
        cp ./../sub_script.sh $dir2
        cd $dir2
		sbatch  --array=1-10 sub_script.sh burn_in_${len}_${h}_${i}.txt $time_end $slim_file $i     
        cd ..

done


        

